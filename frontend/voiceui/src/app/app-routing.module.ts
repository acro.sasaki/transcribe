import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SummaryComponent } from './summary/summary.component';
import { DetailComponent } from './detail/detail.component';
import { RecordComponent } from './record/record.component';


const routes: Routes = [
  {path: "summary", component: SummaryComponent},
  {path: "detail/:uuid_str", component: DetailComponent},
  {path: "record", component: RecordComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
