import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  results: Array<Result>;
  title: String;
  host: String;

  constructor(
    private http: HttpClient, 
    private activateRouter: ActivatedRoute,
    private router: Router
  ) {
    this.host = "https://7f3zix5h9g.execute-api.us-west-2.amazonaws.com/dev/"
    this.results = new Array<Result>();
    this.title = "";
  }

  ngOnInit() {

    this.activateRouter.paramMap.subscribe((params: ParamMap) => {
      const uuid_str = params.get("uuid_str")
      this.getDetailResults(uuid_str);
    })
  }

  getDetailResults(uuid_str) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    let url = this.host + "detail/" + uuid_str;
    this.http.get<Response>(url, httpOptions).subscribe(
      data => {
        this.title = data.title;
        let response = data;
        response.results.forEach(result => {
          this.results.push(result);
        })
      }
    )
  }
}

class Response {
  title: String;
  results: Array<Result>;
}

class Result {
  sentence: String;
  positive: Number;
  negative: Number;
  neutral: Number;
  mixed: Number;
}