import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {

  host: String;
  singedUrl: string;
  fileName: string;
  fileData: File;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private http: HttpClient
    ) {
      this.host = "https://7f3zix5h9g.execute-api.us-west-2.amazonaws.com/dev/";
      console.log(data)
      this.singedUrl = ""
      this.fileName = data.fileName;
      this.fileData = data.fileData;
    }

  ngOnInit() {
     this.getSignedUrl(this.fileName);
  }

  clickUpload() {
    this.uploadFile(this.fileData)
  }

  getSignedUrl(fileName: String) {
    let url = this.host + "signed_url/" + fileName;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'text/html'
      }),
      responseType: "text" as "json"
    };
    this.http.get<any>(url, httpOptions).subscribe(data => {
      console.log(data);
      this.singedUrl = data;
    })
  }

  uploadFile(fileData: File) {
    let xhr = new XMLHttpRequest();
    xhr.open("PUT", this.singedUrl, true);
    xhr.setRequestHeader('Access-Control-Allow-Origin', "*");
    xhr.onload = function () {
      console.log("success");
    }
    xhr.send(fileData);
  }
}
