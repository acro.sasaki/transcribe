import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { UploadDialogComponent } from '../upload-dialog/upload-dialog.component';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit {
  
  isDragOver: boolean;
  
  constructor(
    public dialog: MatDialog
  ) {}

  onDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    return false;
  }

  onDragLeave(evt) {
    this.isDragOver = false;
  }

  onFileDrop(evt) {
    console.log(evt.dataTransfer.files);
    evt.stopPropagation();
    evt.preventDefault();
    let fileData = evt.dataTransfer.files[0];
    let fileName = fileData.name;
    
    this.dialog.open(UploadDialogComponent, {
      data: {
        fileData: fileData,
        fileName: fileName
      }
    })
    this.isDragOver = false;
  }

  onDragEnter(evt) {
    this.isDragOver = true;
  }

  ngOnInit() {
  }

}
