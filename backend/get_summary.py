import json
import boto3
import json

dynamo_client = boto3.client('dynamodb')

def run(event, context):
    """解析結果のサマリ情報を取得する。全体のデータを取得して、ジョブごとに集計した結果を返す。"""

    response = dynamo_client.scan(TableName="transcription_table", ProjectionExpression="title, uuid_str, positive, negative, neutral, mixed")
    items = response["Items"]

    uuid_dict = {}
    for item in items:
        if item["uuid_str"]["S"] in uuid_dict:
            uuid_dict[item["uuid_str"]["S"]]["positive"].append(float(item["positive"]["N"]))
            uuid_dict[item["uuid_str"]["S"]]["negative"].append(float(item["negative"]["N"]))
            uuid_dict[item["uuid_str"]["S"]]["neutral"].append(float(item["neutral"]["N"]))
            uuid_dict[item["uuid_str"]["S"]]["mixed"].append(float(item["mixed"]["N"]))
        else:
            uuid_dict[item["uuid_str"]["S"]] = {}
            uuid_dict[item["uuid_str"]["S"]]["positive"] = [float(item["positive"]["N"])]
            uuid_dict[item["uuid_str"]["S"]]["negative"] = [float(item["negative"]["N"])]
            uuid_dict[item["uuid_str"]["S"]]["neutral"] = [float(item["neutral"]["N"])]
            uuid_dict[item["uuid_str"]["S"]]["mixed"] = [float(item["mixed"]["N"])]
            uuid_dict[item["uuid_str"]["S"]]["title"] = item["title"]["S"]
            
    results = []
    for uuid in uuid_dict:
        results.append(
            {
                "uuid_str": uuid,
                "title": uuid_dict[uuid]["title"],
                "positive": round(sum(uuid_dict[uuid]["positive"])/len(uuid_dict[uuid]["positive"]), 2),
                "negative": round(sum(uuid_dict[uuid]["negative"])/len(uuid_dict[uuid]["negative"]), 2),
                "neutral": round(sum(uuid_dict[uuid]["neutral"])/len(uuid_dict[uuid]["neutral"]), 2),
                "mixed": round(sum(uuid_dict[uuid]["mixed"])/len(uuid_dict[uuid]["mixed"]), 2) 
            }
        )
    
    response = {"results": results}
    return {
        'isBase64Encoded': False,
        "statusCode": "200",
        "headers": {"Access-Control-Allow-Origin": "*"},
        "body": json.dumps(response)
    }